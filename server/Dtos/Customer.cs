﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class Customer
    {
        public string Makh { get; set; }
        public string Tenkh { get; set; }
        public string Diachi { get; set; }
        public string Dienthoai { get; set; }
    }
}
