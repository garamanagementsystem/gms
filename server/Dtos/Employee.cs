﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class Employee
    {
        public string Manv { get; set; }
        public int Id { get; set; }
        public string Hoten { get; set; }
        public string Dienthoai { get; set; }
        public string Diachi { get; set; }
        public string Email { get; set; }
        public string Chucvu { get; set; }
        public decimal? Mucluong { get; set; }
        public DateTime? Ngayvl { get; set; }
    }
}
