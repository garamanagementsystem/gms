﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class PhuT
    {
        public string Mapt { get; set; }
        public string Tenpt { get; set; }
        public decimal Dongia { get; set; }
        public int Soluong { get; set; }

    }
}
