using AutoMapper;
using WebApi.Dtos;
using WebApi.Data;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Users, UserDto>();
            CreateMap<UserDto, Users>();

            CreateMap<Khachhang, Customer>();
            CreateMap<Customer, Khachhang>();

            CreateMap<Nhanvien, Employee>();
            CreateMap<Employee, Nhanvien>();

            CreateMap<Phutung, PhuT>();
            CreateMap<PhuT, Phutung>();

            CreateMap<HieuX, Hieuxe>();
            CreateMap<Hieuxe, HieuX>();
        }     
    }
}