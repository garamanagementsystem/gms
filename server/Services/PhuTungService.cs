﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Data;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IPhuTungService
    {
        IEnumerable<Phutung> GetAll();
        Phutung Create(Phutung customer);
        void Update(Phutung customer);
    }
    public class PhuTungService : IPhuTungService
    {
        private QUANLYGARAContext _context;
        private IMapper _mapper;
        public PhuTungService(QUANLYGARAContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public IEnumerable<Phutung> GetAll()
        {
            return (_context.Phutung);
        }

        public Phutung Create(Phutung phutung)
        {
            if (string.IsNullOrWhiteSpace(phutung.Mapt))
                throw new AppException("mapt is required");
            if (string.IsNullOrWhiteSpace(phutung.Tenpt))
                throw new AppException("Tenpt is required");
            if (_context.Phutung.Any(x => x.Mapt == phutung.Mapt))
                throw new AppException("mapt " + phutung.Mapt + " is already taken");
            _context.Phutung.Add(phutung);
            _context.SaveChanges();
            return phutung;
        }
        public void Update(Phutung phutung)
        {
            if (string.IsNullOrWhiteSpace(phutung.Mapt))
                throw new AppException("mapt is required");
            var pt = _context.Phutung.Where(x => x.Mapt == phutung.Mapt).FirstOrDefault();
            pt.Tenpt = phutung.Tenpt;
            pt.Soluong = phutung.Soluong;
            pt.Dongia = phutung.Dongia;
            _context.Phutung.Update(pt);
            _context.SaveChanges();
        }
    }
}
