﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Data;
using WebApi.Helpers;

namespace WebApi.Services
{

    public interface IHieuXeService
    {
        IEnumerable<Hieuxe> GetAll();
        Hieuxe Create(Hieuxe customer);
        void Update(Hieuxe customer);
    }
    public class HieuXeService : IHieuXeService
    {
        private QUANLYGARAContext _context;
        private IMapper _mapper;
        public HieuXeService(QUANLYGARAContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public IEnumerable<Hieuxe> GetAll()
        {
            return (_context.Hieuxe);
        }

        public Hieuxe Create(Hieuxe Hieuxe)
        {
            if (string.IsNullOrWhiteSpace(Hieuxe.Mahx))
                throw new AppException("mahx is required");
            if (string.IsNullOrWhiteSpace(Hieuxe.Tenhx))
                throw new AppException("tenhx is required");
            _context.Hieuxe.Add(Hieuxe);
            _context.SaveChanges();
            return Hieuxe;
        }
        public void Update(Hieuxe Hieuxe)
        {
            if (string.IsNullOrWhiteSpace(Hieuxe.Mahx))
                throw new AppException("mahx is required");
            var pt = _context.Hieuxe.Where(x => x.Mahx == Hieuxe.Mahx).FirstOrDefault();
            pt.Tenhx = Hieuxe.Tenhx;
            _context.Hieuxe.Update(pt);
            _context.SaveChanges();
        }
    }

}
