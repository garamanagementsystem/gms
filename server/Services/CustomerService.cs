﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ICustomerService
    {
        IEnumerable<Khachhang> GetAll();
        Khachhang Create(Khachhang customer);
        void Update(Khachhang customer);
        void Delete(string makh);
    }
    public class CustomerService : ICustomerService
    {
        private QUANLYGARAContext _context;
        private IMapper _mapper;
        public CustomerService(QUANLYGARAContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public IEnumerable<Khachhang> GetAll()
        {
            return (_context.Khachhang);
        }
        public Khachhang Create(Khachhang customer)
        {
            if (string.IsNullOrWhiteSpace(customer.Makh))
                throw new AppException("MAKH is required");
            if (string.IsNullOrWhiteSpace(customer.Tenkh))
                throw new AppException("TenKH is required");
            if (_context.Khachhang.Any(x => x.Makh == customer.Makh))
                throw new AppException("MAKH " + customer.Makh + " is already taken");
            _context.Khachhang.Add(customer);
            _context.SaveChanges();
            return customer;
        }
        public void Update(Khachhang customer)
        {
            if (string.IsNullOrWhiteSpace(customer.Tenkh))
                throw new AppException("TenKH is required");
            var khach = _context.Khachhang.Where(x => x.Makh == customer.Makh).FirstOrDefault();
            khach.Tenkh = customer.Tenkh;
            khach.Diachi = customer.Diachi;
            khach.Dienthoai = customer.Dienthoai;
            _context.Khachhang.Update(khach);
            _context.SaveChanges();
        }
        public void Delete(string makh)
        {
            var customer = _context.Khachhang.Where(x => x.Makh == makh).FirstOrDefault();
            if (customer != null)
            {
                _context.Khachhang.Remove(customer);
                _context.SaveChanges();
            }
        }
    }
}
