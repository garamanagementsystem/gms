﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class CustomersController : Controller
    {
        private ICustomerService _customerrService;
        private IMapper _mapper;

        public CustomersController(
            ICustomerService customerrService,
            IMapper mapper)
        {
            _customerrService = customerrService;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _customerrService.GetAll();
            var usersList = _mapper.Map<IList<Customer>>(users);
            return Ok(usersList);
        }
        [HttpPut]
        public IActionResult Update([FromBody]Customer customerDto)
        {
            // map dto to entity and set id
            var customer = _mapper.Map<Khachhang>(customerDto);

            try
            {
                // save 
                _customerrService.Update(customer);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string makh)
        {
            _customerrService.Delete(makh);
            return Ok();
        }
    }
}