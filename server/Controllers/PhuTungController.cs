﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class PhuTungController : Controller
    {
        private IPhuTungService _phuTungService;
        private IMapper _mapper;

        public PhuTungController(
            IPhuTungService phuTungService,
            IMapper mapper

            )
        {
            _phuTungService = phuTungService;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var pt = _phuTungService.GetAll();
            var pts = _mapper.Map<IList<PhuT>>(pt);
            return Ok(pts);
        }

        [HttpPost]
        public IActionResult Create([FromBody]PhuT pt)
        {
            var newpt = _mapper.Map<Phutung>(pt);
            var result = _phuTungService.Create(newpt);
            return Ok(result);
        }

        [HttpPut]
        public IActionResult Update([FromBody]PhuT pt)
        {
            var newpt = _mapper.Map<Phutung>(pt);
            _phuTungService.Update(newpt);
            return Ok();
        }
    }
}
