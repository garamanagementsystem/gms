﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class HieuXeController : Controller
    {
        private IHieuXeService _hieuXeService;
        private IMapper _mapper;

        public HieuXeController(
            IHieuXeService hieuXeService,
            IMapper mapper

            )
        {
            _hieuXeService = hieuXeService;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var pt = _hieuXeService.GetAll();
            var pts = _mapper.Map<IList<HieuX>>(pt);
            return Ok(pts);
        }

        [HttpPost]
        public IActionResult Create([FromBody]HieuX pt)
        {
            var newpt = _mapper.Map<Hieuxe>(pt);
            var result = _hieuXeService.Create(newpt);
            return Ok(result);
        }

        [HttpPut]
        public IActionResult Update([FromBody]HieuX pt)
        {
            var newpt = _mapper.Map<Hieuxe>(pt);
            _hieuXeService.Update(newpt);
            return Ok();
        }
    }
}
