﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Nhanvien
    {
        public Nhanvien()
        {
            Hoadon = new HashSet<Hoadon>();
        }

        public string Manv { get; set; }
        public int Id { get; set; }
        public string Hoten { get; set; }
        public string Dienthoai { get; set; }
        public string Diachi { get; set; }
        public string Email { get; set; }
        public string Chucvu { get; set; }
        public decimal? Mucluong { get; set; }
        public DateTime? Ngayvl { get; set; }

        public Users IdNavigation { get; set; }
        public ICollection<Hoadon> Hoadon { get; set; }
    }
}
