﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Phutung
    {
        public Phutung()
        {
            Chitiethd = new HashSet<Chitiethd>();
            Ttphieusuachua = new HashSet<Ttphieusuachua>();
        }

        public string Mapt { get; set; }
        public string Tenpt { get; set; }
        public decimal Dongia { get; set; }
        public int Soluong { get; set; }

        public ICollection<Chitiethd> Chitiethd { get; set; }
        public ICollection<Ttphieusuachua> Ttphieusuachua { get; set; }
    }
}
