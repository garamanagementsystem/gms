﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Ttphieusuachua
    {
        public string Mapsc { get; set; }
        public string Mapt { get; set; }
        public string Noidung { get; set; }
        public int Soluong { get; set; }
        public decimal Tiencong { get; set; }

        public Phieusuachua MapscNavigation { get; set; }
        public Phutung MaptNavigation { get; set; }
    }
}
