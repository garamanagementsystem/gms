﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Ttxe
    {
        public Ttxe()
        {
            Hoadon = new HashSet<Hoadon>();
        }

        public string Mattxe { get; set; }
        public string Makh { get; set; }
        public string Mahx { get; set; }
        public string Bienso { get; set; }
        public string Tenxe { get; set; }

        public Hieuxe MahxNavigation { get; set; }
        public Khachhang MakhNavigation { get; set; }
        public ICollection<Hoadon> Hoadon { get; set; }
    }
}
