﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Hieuxe
    {
        public Hieuxe()
        {
            Ttxe = new HashSet<Ttxe>();
        }

        public string Mahx { get; set; }
        public string Tenhx { get; set; }

        public ICollection<Ttxe> Ttxe { get; set; }
    }
}
