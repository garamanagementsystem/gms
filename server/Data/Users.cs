﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Users
    {
        public Users()
        {
            Nhanvien = new HashSet<Nhanvien>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Permission { get; set; }

        public ICollection<Nhanvien> Nhanvien { get; set; }
    }
}
