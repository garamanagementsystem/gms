﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApi.Data
{
    public partial class QUANLYGARAContext : DbContext
    {
        public virtual DbSet<Chitiethd> Chitiethd { get; set; }
        public virtual DbSet<Hieuxe> Hieuxe { get; set; }
        public virtual DbSet<Hoadon> Hoadon { get; set; }
        public virtual DbSet<Khachhang> Khachhang { get; set; }
        public virtual DbSet<Nhanvien> Nhanvien { get; set; }
        public virtual DbSet<Phieusuachua> Phieusuachua { get; set; }
        public virtual DbSet<Phieutiepnhan> Phieutiepnhan { get; set; }
        public virtual DbSet<Phutung> Phutung { get; set; }
        public virtual DbSet<Ttphieusuachua> Ttphieusuachua { get; set; }
        public virtual DbSet<Ttxe> Ttxe { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        public QUANLYGARAContext(DbContextOptions<QUANLYGARAContext> options)
    : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chitiethd>(entity =>
            {
                entity.HasKey(e => new { e.Mahd, e.Mapt });

                entity.ToTable("CHITIETHD");

                entity.Property(e => e.Mahd)
                    .HasColumnName("MAHD")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Mapt)
                    .HasColumnName("MAPT")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Soluong).HasColumnName("SOLUONG");

                entity.HasOne(d => d.MahdNavigation)
                    .WithMany(p => p.Chitiethd)
                    .HasForeignKey(d => d.Mahd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CHITIETHD_HOADON");

                entity.HasOne(d => d.MaptNavigation)
                    .WithMany(p => p.Chitiethd)
                    .HasForeignKey(d => d.Mapt)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CHITIETHD_PHUTUNG");
            });

            modelBuilder.Entity<Hieuxe>(entity =>
            {
                entity.HasKey(e => e.Mahx);

                entity.ToTable("HIEUXE");

                entity.Property(e => e.Mahx)
                    .HasColumnName("MAHX")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Tenhx)
                    .IsRequired()
                    .HasColumnName("TENHX")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Hoadon>(entity =>
            {
                entity.HasKey(e => e.Mahd);

                entity.ToTable("HOADON");

                entity.Property(e => e.Mahd)
                    .HasColumnName("MAHD")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Makh)
                    .IsRequired()
                    .HasColumnName("MAKH")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Manv)
                    .IsRequired()
                    .HasColumnName("MANV")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mattxe)
                    .IsRequired()
                    .HasColumnName("MATTXE")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Ngaylap)
                    .HasColumnName("NGAYLAP")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Tongtien)
                    .HasColumnName("TONGTIEN")
                    .HasColumnType("money");

                entity.HasOne(d => d.ManvNavigation)
                    .WithMany(p => p.Hoadon)
                    .HasForeignKey(d => d.Manv)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HOADON_NHANVIEN");

                entity.HasOne(d => d.MattxeNavigation)
                    .WithMany(p => p.Hoadon)
                    .HasForeignKey(d => d.Mattxe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HOADON_TTXE");
            });

            modelBuilder.Entity<Khachhang>(entity =>
            {
                entity.HasKey(e => e.Makh);

                entity.ToTable("KHACHHANG");

                entity.Property(e => e.Makh)
                    .HasColumnName("MAKH")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Diachi)
                    .IsRequired()
                    .HasColumnName("DIACHI")
                    .HasMaxLength(500);

                entity.Property(e => e.Dienthoai)
                    .IsRequired()
                    .HasColumnName("DIENTHOAI")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Tenkh)
                    .IsRequired()
                    .HasColumnName("TENKH")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Nhanvien>(entity =>
            {
                entity.HasKey(e => e.Manv);

                entity.ToTable("NHANVIEN");

                entity.Property(e => e.Manv)
                    .HasColumnName("MANV")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Chucvu)
                    .HasColumnName("CHUCVU")
                    .HasMaxLength(50);

                entity.Property(e => e.Diachi)
                    .HasColumnName("DIACHI")
                    .HasMaxLength(500);

                entity.Property(e => e.Dienthoai)
                    .HasColumnName("DIENTHOAI")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50);

                entity.Property(e => e.Hoten)
                    .IsRequired()
                    .HasColumnName("HOTEN")
                    .HasMaxLength(50);

                entity.Property(e => e.Mucluong)
                    .HasColumnName("MUCLUONG")
                    .HasColumnType("money");

                entity.Property(e => e.Ngayvl)
                    .HasColumnName("NGAYVL")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.Nhanvien)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NHANVIEN_Users");
            });

            modelBuilder.Entity<Phieusuachua>(entity =>
            {
                entity.HasKey(e => e.Mapsc);

                entity.ToTable("PHIEUSUACHUA");

                entity.Property(e => e.Mapsc)
                    .HasColumnName("MAPSC")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Maphieutn)
                    .IsRequired()
                    .HasColumnName("MAPHIEUTN")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Ngaysc)
                    .HasColumnName("NGAYSC")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.MaphieutnNavigation)
                    .WithMany(p => p.Phieusuachua)
                    .HasForeignKey(d => d.Maphieutn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PHIEUSUACHUA_PHIEUTIEPNHAN");
            });

            modelBuilder.Entity<Phieutiepnhan>(entity =>
            {
                entity.HasKey(e => e.Maphieutn);

                entity.ToTable("PHIEUTIEPNHAN");

                entity.Property(e => e.Maphieutn)
                    .HasColumnName("MAPHIEUTN")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Manv)
                    .IsRequired()
                    .HasColumnName("MANV")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mattxe)
                    .IsRequired()
                    .HasColumnName("MATTXE")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Ngaytn)
                    .HasColumnName("NGAYTN")
                    .HasColumnType("smalldatetime");
            });

            modelBuilder.Entity<Phutung>(entity =>
            {
                entity.HasKey(e => e.Mapt);

                entity.ToTable("PHUTUNG");

                entity.Property(e => e.Mapt)
                    .HasColumnName("MAPT")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Dongia)
                    .HasColumnName("DONGIA")
                    .HasColumnType("money");

                entity.Property(e => e.Soluong).HasColumnName("SOLUONG");

                entity.Property(e => e.Tenpt)
                    .IsRequired()
                    .HasColumnName("TENPT")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Ttphieusuachua>(entity =>
            {
                entity.HasKey(e => new { e.Mapsc, e.Mapt });

                entity.ToTable("TTPHIEUSUACHUA");

                entity.Property(e => e.Mapsc)
                    .HasColumnName("MAPSC")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Mapt)
                    .HasColumnName("MAPT")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Noidung)
                    .HasColumnName("NOIDUNG")
                    .HasMaxLength(1000);

                entity.Property(e => e.Soluong).HasColumnName("SOLUONG");

                entity.Property(e => e.Tiencong)
                    .HasColumnName("TIENCONG")
                    .HasColumnType("money");

                entity.HasOne(d => d.MapscNavigation)
                    .WithMany(p => p.Ttphieusuachua)
                    .HasForeignKey(d => d.Mapsc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TTPHIEUSUACHUA_PHIEUSUACHUA");

                entity.HasOne(d => d.MaptNavigation)
                    .WithMany(p => p.Ttphieusuachua)
                    .HasForeignKey(d => d.Mapt)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TTPHIEUSUACHUA_PHUTUNG");
            });

            modelBuilder.Entity<Ttxe>(entity =>
            {
                entity.HasKey(e => e.Mattxe);

                entity.ToTable("TTXE");

                entity.Property(e => e.Mattxe)
                    .HasColumnName("MATTXE")
                    .HasColumnType("char(5)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bienso)
                    .IsRequired()
                    .HasColumnName("BIENSO")
                    .HasColumnType("char(9)");

                entity.Property(e => e.Mahx)
                    .IsRequired()
                    .HasColumnName("MAHX")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Makh)
                    .IsRequired()
                    .HasColumnName("MAKH")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Tenxe)
                    .HasColumnName("TENXE")
                    .HasColumnType("nchar(20)");

                entity.HasOne(d => d.MahxNavigation)
                    .WithMany(p => p.Ttxe)
                    .HasForeignKey(d => d.Mahx)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TTXE_MAHX_HIEUXE");

                entity.HasOne(d => d.MakhNavigation)
                    .WithMany(p => p.Ttxe)
                    .HasForeignKey(d => d.Makh)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TTXE_MAKH_KHACHHANG");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Permission)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("(N'nhanvien')");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
