﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Khachhang
    {
        public Khachhang()
        {
            Ttxe = new HashSet<Ttxe>();
        }

        public string Makh { get; set; }
        public string Tenkh { get; set; }
        public string Diachi { get; set; }
        public string Dienthoai { get; set; }

        public ICollection<Ttxe> Ttxe { get; set; }
    }
}
