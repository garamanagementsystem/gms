﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Phieutiepnhan
    {
        public Phieutiepnhan()
        {
            Phieusuachua = new HashSet<Phieusuachua>();
        }

        public string Maphieutn { get; set; }
        public string Mattxe { get; set; }
        public string Manv { get; set; }
        public DateTime Ngaytn { get; set; }

        public ICollection<Phieusuachua> Phieusuachua { get; set; }
    }
}
