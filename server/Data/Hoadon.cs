﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Hoadon
    {
        public Hoadon()
        {
            Chitiethd = new HashSet<Chitiethd>();
        }

        public string Mahd { get; set; }
        public string Manv { get; set; }
        public string Makh { get; set; }
        public string Mattxe { get; set; }
        public DateTime Ngaylap { get; set; }
        public decimal Tongtien { get; set; }

        public Nhanvien ManvNavigation { get; set; }
        public Ttxe MattxeNavigation { get; set; }
        public ICollection<Chitiethd> Chitiethd { get; set; }
    }
}
