﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Chitiethd
    {
        public string Mahd { get; set; }
        public string Mapt { get; set; }
        public int? Soluong { get; set; }

        public Hoadon MahdNavigation { get; set; }
        public Phutung MaptNavigation { get; set; }
    }
}
