﻿using System;
using System.Collections.Generic;

namespace WebApi.Data
{
    public partial class Phieusuachua
    {
        public Phieusuachua()
        {
            Ttphieusuachua = new HashSet<Ttphieusuachua>();
        }

        public string Mapsc { get; set; }
        public string Maphieutn { get; set; }
        public DateTime Ngaysc { get; set; }

        public Phieutiepnhan MaphieutnNavigation { get; set; }
        public ICollection<Ttphieusuachua> Ttphieusuachua { get; set; }
    }
}
