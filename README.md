0/ How to pull code: git -c http.sslVerify=false clone https://doanis208@bitbucket.org/garamanagementsystem/gms.git

---------- PLEASE PUSH YOUR CODE TO A NEW BRANCH, not push code to development || master ------------------------------------
After push code to your branch please create a merge request to merge your branch to master branch

1/ Resources:

NetCore 2.0: https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.105-windows-x64-installer
NodeJs: https://nodejs.org/dist/v8.11.1/node-v8.11.1-x64.msi

2/ Software: Visual Code, SQL server 2017, Visual studio 2017

3/ How to run? 

Step 1: Run script DBScript.sql in Database Folder for create DB.

Step 2: Update connectionString at server/appsettings.json

Step 3: Open CMD and navigate to folder client. Type "npm install" for install package to done then Type "npm start" for start front-end

Step 4: Open CMD and navigate to folder server. Type "dotnet restore" to done then Type "dotnet run"

Step 5: Open browser and navigate to http://localhost:3000

Account/Passwork: admin/admin