﻿import { DataPhuTungComponent } from './data/data-phutung/data-phutung.component';
import { TabStateService } from './_services/tab-state.service';
import { SearchComponent } from './task/search/search.component';
import { ReportComponent } from './report/report.component';
import { TaskComponent } from './task/task.component';
import { AddNewEmployeeComponent } from './admin/add-new-employee/add-new-employee.component';
import { AdminComponent } from './admin/admin.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AppConfig } from './app.config';

import { ClarityModule } from '@clr/angular';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService, HieuXeService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { LoadingPageComponent } from './common/loading-page/loading-page.component';
import { ToastComponent } from './common/toast-notification/toast-notification.component';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DataComponent } from './data/data.component';
import { CustomerComponent } from './admin/customer/customer.component';
import { CustomerService } from './_services/customer.service';
import { PhuTungService } from './_services/phutung.service';
import { DataHieuXeComponent } from './data/data-hieuxe/data-hieuxe.component';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        ClarityModule,
        ToastModule.forRoot(),
        BrowserAnimationsModule,
        NoopAnimationsModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AdminComponent,
        AddNewEmployeeComponent,
        TaskComponent,
        ReportComponent,
        DataComponent,
        LoadingPageComponent,
        ToastComponent,
        SearchComponent,
        DataPhuTungComponent,
        CustomerComponent,
        DataHieuXeComponent
    ],
    providers: [
        AppConfig,
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        TabStateService,
        CustomerService,
        PhuTungService,
        HieuXeService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
