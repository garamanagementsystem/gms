export class Employee {
    public manv: string;
    public id: number;
    public hoten: string;
    public dienthoai: string;
    public diachi: string;
    public email: string;
    public chucvu: string;
    public mucluong: number;
    public ngayvl: string;
}
