﻿import { ReportComponent } from "./report/report.component";
import { TaskComponent } from "./task/task.component";
import { AdminComponent } from "./admin/admin.component";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/index";
import { LoginComponent } from "./login/index";
import { RegisterComponent } from "./register/index";
import { AuthGuard } from "./_guards/index";
import { DataComponent } from './data/data.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "admin", component: AdminComponent, canActivate: [AuthGuard] },
  { path: "task", component: TaskComponent, canActivate: [AuthGuard] },
  { path: "report", component: ReportComponent, canActivate: [AuthGuard] },
  { path: "data", component: DataComponent, canActivate: [AuthGuard] },
  // otherwise redirect to home
  { path: "**", redirectTo: "" }
];

export const routing = RouterModule.forRoot(appRoutes);
