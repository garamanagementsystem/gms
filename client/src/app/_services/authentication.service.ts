﻿import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { AppConfig } from "../app.config";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AuthenticationService {
  private loggedIn = new BehaviorSubject<boolean>(false);
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  constructor(private http: Http, private config: AppConfig) {}

  public login(username: string, password: string) {
    return this.http
      .post(this.config.apiUrl + "/users/authenticate", {
        username: username,
        password: password
      })
      .map((response: Response) => {
        let user = response.json();
        if (user && user.token) {
          localStorage.setItem("currentUser", JSON.stringify(user));
        }
        this.loggedIn.next(true);
      });
  }

  public checkLogin(): void {
    if (localStorage.getItem("currentUser") != null) this.loggedIn.next(true);
    else this.loggedIn.next(false);
  }

  public logout(): void {
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    this.loggedIn.next(false);
  }
}
