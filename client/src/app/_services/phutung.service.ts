import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { Customer } from '../_models/index';
import { PhuTung } from '../_models/phutung.model';

@Injectable()
export class PhuTungService {
    constructor(private http: Http, private config: AppConfig) { }

    getAll() {
        return this.http.get(this.config.apiUrl + 'phutung', this.jwt()).map((response: Response) => response.json());
    }

    create(pt: PhuTung) {
        return this.http.post(this.config.apiUrl + '/phutung', pt, this.jwt());
    }

    update(pt: PhuTung) {
        return this.http.put(this.config.apiUrl + '/phutung' , pt, this.jwt());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            const headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
