import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { HieuXe } from '../_models/hieuxe.model';

@Injectable()
export class HieuXeService {
    constructor(private http: Http, private config: AppConfig) { }

    getAll() {
        return this.http.get(this.config.apiUrl + 'hieuxe', this.jwt()).map((response: Response) => response.json());
    }

    create(pt: HieuXe) {
        return this.http.post(this.config.apiUrl + '/hieuxe', pt, this.jwt());
    }

    update(pt: HieuXe) {
        return this.http.put(this.config.apiUrl + '/hieuxe', pt, this.jwt());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            const headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
