import { Injectable } from '@angular/core';

@Injectable()
export class TabStateService {
    public getTabState(componentName: string): number {
        return Number(localStorage.getItem(componentName));
    }

    public setTabState(componentName: string, tabIndex: number): void {
        localStorage.setItem(componentName, tabIndex.toString());
    }
}
