import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../app.config';
import { Customer } from '../_models/index';

@Injectable()
export class CustomerService {
    constructor(private http: Http, private config: AppConfig) { }

    getAll() {
        return this.http.get(this.config.apiUrl + 'customers', this.jwt()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get(this.config.apiUrl + '/customers/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create(customer: Customer) {
        return this.http.post(this.config.apiUrl + '/customers', customer, this.jwt());
    }

    update(customer: Customer) {
        return this.http.put(this.config.apiUrl + '/customers' , customer, this.jwt());
    }

    delete(makh: string) {
        return this.http.delete(this.config.apiUrl + '/customers/' + makh, this.jwt());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            const headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
