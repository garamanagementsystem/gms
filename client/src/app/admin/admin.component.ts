import { Component, OnInit, ViewContainerRef } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { JQueryHelper } from '../common/helpers/jquery.helper';
import { Employee } from '../_models/employee';
import { ToastComponent } from '../common/toast-notification/toast-notification.component';
import { ToastsManager } from 'ng2-toastr';

@Component({
    moduleId: module.id,
    templateUrl: 'admin.component.html'
})
export class AdminComponent implements OnInit {
    private addNewUser = false;
    private editMode = false;
    private idEmployeeSelected: number;
    public currentUser: User = new User();
    public currentEmployee: Employee = new Employee();
    public cloneEmployee: Employee = new Employee();
    public users: User[] = [];
    public tab1: boolean = true;
    public tab2: boolean = false;
    private toastComponent: ToastComponent;

    constructor(private userService: UserService, private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastComponent = new ToastComponent(toastr, vcr);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.idEmployeeSelected = this.currentUser.id - 1;
    }

    public ngOnInit() {
        JQueryHelper.showLoading();
        this.loadAllUsers();
    }
    public toggle1() {
        this.tab1 = true;
        this.tab2 = false;
    }
    public toggle2() {
        this.tab1 = false;
        this.tab2 = true;
    }
    private deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => {
            this.loadAllUsers();
        });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe((users) => {
            this.users = users;
            this.userService.getNhanVienById(this.currentUser.id).subscribe((res) => {
                if (res) {
                    this.currentEmployee = res;
                    console.log(this.currentEmployee);
                }
            });
            JQueryHelper.hideLoading();
        });
    }
    private selectUser(i: number) {
        this.idEmployeeSelected = i;
        this.userService.getNhanVienById(this.users[this.idEmployeeSelected].id).subscribe((res) => {
            if (res) {
                this.currentEmployee = res;
            }
        });
    }
    private onEdit() {
        this.cloneEmployee = Object.assign({}, this.currentEmployee);
        this.editMode = true;
    }
    private onCancel() {
        this.currentEmployee = Object.assign({}, this.cloneEmployee);
        this.editMode = false;
    }
    private onSave() {
        JQueryHelper.showLoading();
        console.log(this.currentEmployee);
        this.userService.updateNhanVien(this.currentEmployee).subscribe((x) => {
            JQueryHelper.hideLoading();
            if (x) {
                this.toastComponent.showSuccess('Cập nhật thông tin nhân viên thành công!');
                this.editMode = false;
            } else {
                this.toastComponent.showError('Cập nhật thông tin của nhân viên thất bại!');
            }
        });
    }
    private closeModal(success: boolean) {
        if (success) {
            this.loadAllUsers();
        }
        this.addNewUser = false;
    }
}
