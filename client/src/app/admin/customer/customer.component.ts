import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Customer } from '../../_models';
import { CustomerService } from '../../_services';
import { JQueryHelper } from '../../common/helpers/jquery.helper';
import { ToastComponent } from '../../common/toast-notification/toast-notification.component';
import { ToastsManager } from 'ng2-toastr';

@Component({
    moduleId: module.id,
    selector: 'customer',
    templateUrl: 'customer.component.html'
})
export class CustomerComponent implements OnInit {
    private addNewCustomer = false;
    private editMode = false;
    private idCustomerSelected: number;
    public currentCustomer: Customer = new Customer();
    public Customers: Customer[] = [];
    private tab1: boolean = true;
    private tab2: boolean = false;
    private loading: boolean = true;
    private toastComponent: ToastComponent;


    constructor(private customerService: CustomerService, private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastComponent = new ToastComponent(toastr, vcr);
    }

    public ngOnInit() {
        JQueryHelper.showLoading();
        this.loadAllCustomers();
    }

    private deleteCustomer(makh: string) {
        this.customerService.delete(makh).subscribe(() => {
            this.loadAllCustomers();
        });
    }

    private loadAllCustomers() {
        this.customerService.getAll().subscribe((Customers) => {
            this.Customers = Customers;
            this.currentCustomer = this.Customers[0];
            this.loading = false;
            JQueryHelper.hideLoading();
        });
    }
    private selectCustomer(i: number) {
        this.currentCustomer = Object.assign({}, this.Customers[i]);
        this.idCustomerSelected = i;
    }
    private onEdit() {
        this.currentCustomer = Object.assign({}, this.Customers[this.idCustomerSelected]);
        this.editMode = true;
    }
    private onCancel() {
        this.currentCustomer = Object.assign({}, this.Customers[this.idCustomerSelected]);
        this.editMode = false;
    }
    private closeModal(success: boolean) {
        if (success) {
            this.loadAllCustomers();
        }
        this.addNewCustomer = false;
    }
    public onSave() {
        JQueryHelper.showLoading();
        this.customerService.update(this.currentCustomer).subscribe((res) => {
            if (res) {
                JQueryHelper.hideLoading();
                this.toastComponent.showSuccess('Cập nhật khách hàng thành công!');
                this.loadAllCustomers();
                this.editMode = false;
            } else {
                JQueryHelper.hideLoading();
                this.toastComponent.showError('Cập nhật khách hàng không thành công!');
            }
        });
    }
}
