import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../../_services/user.service";

@Component({
  moduleId: module.id,
  selector: "add-new-employee",
  templateUrl: "add-new-employee.component.html"
})
export class AddNewEmployeeComponent implements OnInit {
  @Output("close") private onClose = new EventEmitter<boolean>();
  model: any = {};
  success = false;
  message: string = "";

  constructor(private router: Router, private userService: UserService) {}

  register() {
    this.userService.create(this.model).subscribe(
      data => {
        this.message = "Add a employee Success";
        this.success = true;
      },
      error => {
        this.message = error._body;
        this.success = false;
      }
    );
  }
  public ngOnInit() {}

  public close(): void {
    this.onClose.emit(this.success);
  }

  public validateForm(): boolean {
    if (this.model == null ||
      this.model.firstName == null || this.model.firstName.trim() == "" ||
      this.model.lastName == null || this.model.lastName.trim() == "" ||
      this.model.username == null || this.model.username.trim() == "" ||
      this.model.password == null || this.model.password.trim() == ""
    ) {
      return false;
    }
    return true;
  }
}
