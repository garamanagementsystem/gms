﻿import { Component, OnInit } from '@angular/core';
import { User } from './_models';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './_services/authentication.service';
import * as $ from 'jquery';
import * as moment from 'moment';

@Component({
  moduleId: module.id,
  selector: 'app',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  private hideNav: boolean = true;
  currentUser: User;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authenticationService.isLoggedIn;
    this.authenticationService.checkLogin();
  }

  private logout() {
    this.authenticationService.logout();
  }
}
