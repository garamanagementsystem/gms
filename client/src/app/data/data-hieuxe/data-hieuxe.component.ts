import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { JQueryHelper } from '../../common/helpers/jquery.helper';
import { ToastsManager } from 'ng2-toastr';
import { ToastComponent } from '../../common/toast-notification/toast-notification.component';
import { HieuXeService } from '../../_services/hieuxe.service';
import { HieuXe } from '../../_models';
@Component({
    moduleId: module.id,
    selector: 'hieu-xe',
    templateUrl: 'data-hieuXe.component.html'
})
export class DataHieuXeComponent implements OnInit {
    private hieuxes: HieuXe[];
    private currenthieuxe: HieuXe = new HieuXe();
    private toastComponent: ToastComponent;
    private idhieuxeSelected: number;
    private editMode: boolean = false;
    private flag: boolean = false;
    constructor(private hieuxeService: HieuXeService, private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastComponent = new ToastComponent(toastr, vcr);
    }

    public ngOnInit() {
        JQueryHelper.showLoading();
        this.loadAllhieuxes();
    }

    private loadAllhieuxes() {
        this.hieuxeService.getAll().subscribe((hx) => {
            this.hieuxes = hx;
            this.currenthieuxe = this.hieuxes[0];
            JQueryHelper.hideLoading();
        });
    }
    private selecthieuxe(i: number) {
        this.currenthieuxe = Object.assign({}, this.hieuxes[i]);
        this.idhieuxeSelected = i;
    }
    private onEdit() {
        this.currenthieuxe = Object.assign({}, this.hieuxes[this.idhieuxeSelected]);
        this.editMode = true;
    }
    private onCancel() {
        this.currenthieuxe = Object.assign({}, this.hieuxes[this.idhieuxeSelected]);
        this.editMode = false;
    }
    private onAddHieuXe() {
        this.editMode = true;
        this.currenthieuxe = new HieuXe();
        this.flag = true;
    }
    public onSave() {
        JQueryHelper.showLoading();
        if (!this.flag) {
            this.hieuxeService.update(this.currenthieuxe).subscribe((res) => {
                if (res) {
                    JQueryHelper.hideLoading();
                    this.toastComponent.showSuccess('Cập nhật hiệu xe thành công!');
                    this.loadAllhieuxes();
                    this.editMode = false;
                } else {
                    JQueryHelper.hideLoading();
                    this.toastComponent.showError('Cập nhật hiệu xe không thành công!');
                }
            });
        } else {
            this.hieuxeService.create(this.currenthieuxe).subscribe((res) => {
                if (res) {
                    JQueryHelper.hideLoading();
                    this.toastComponent.showSuccess('Thêm hiệu xe thành công!');
                    this.loadAllhieuxes();
                    this.editMode = false;
                } else {
                    JQueryHelper.hideLoading();
                    this.toastComponent.showError('Thêm hiệu xe không thành công!');
                }
                this.flag = false;
            });
        }
    }
}
