import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { JQueryHelper } from '../../common/helpers/jquery.helper';
import { PhuTungService } from '../../_services/phutung.service';
import { ToastsManager } from 'ng2-toastr';
import { ToastComponent } from '../../common/toast-notification/toast-notification.component';
import { PhuTung } from '../../_models/phutung.model';
@Component({
  moduleId: module.id,
  selector: 'phu-tung',
  templateUrl: 'data-phutung.component.html'
})
export class DataPhuTungComponent implements OnInit {
  private phutungs: PhuTung[];
  private currentphutung: PhuTung = new PhuTung();
  private toastComponent: ToastComponent;
  private idphutungSelected: number;
  private editMode: boolean = false;
  private flag: boolean = false;
  constructor(private phuTungService: PhuTungService, private toastr: ToastsManager,
    private vcr: ViewContainerRef) {
    this.toastComponent = new ToastComponent(toastr, vcr);
  }

  public ngOnInit() {
    JQueryHelper.showLoading();
    this.loadAllphutungs();
  }

  private loadAllphutungs() {
    this.phuTungService.getAll().subscribe((phutung) => {
      this.phutungs = phutung;
      this.currentphutung = this.phutungs[0];
      JQueryHelper.hideLoading();
    });
  }
  private selectphutung(i: number) {
    this.currentphutung = Object.assign({}, this.phutungs[i]);
    this.idphutungSelected = i;
  }
  private onEdit() {
    this.currentphutung = Object.assign({}, this.phutungs[this.idphutungSelected]);
    this.editMode = true;
  }
  private onCancel() {
    this.currentphutung = Object.assign({}, this.phutungs[this.idphutungSelected]);
    this.editMode = false;
  }
  private onAddPhuTung() {
    this.editMode = true;
    this.currentphutung = new PhuTung();
    this.flag = true;
  }
  public onSave() {
    console.log(this.currentphutung);
    JQueryHelper.showLoading();
    if (!this.flag) {
      this.phuTungService.update(this.currentphutung).subscribe((res) => {
        if (res) {
          JQueryHelper.hideLoading();
          this.toastComponent.showSuccess('Cập nhật phụ tùng thành công!');
          this.loadAllphutungs();
          this.editMode = false;
        } else {
          JQueryHelper.hideLoading();
          this.toastComponent.showError('Cập nhật phụ tùng không thành công!');
        }
      });
    } else {
      this.phuTungService.create(this.currentphutung).subscribe((res) => {
        if (res) {
          JQueryHelper.hideLoading();
          this.toastComponent.showSuccess('Thêm phụ tùng thành công!');
          this.loadAllphutungs();
          this.editMode = false;
        } else {
          JQueryHelper.hideLoading();
          this.toastComponent.showError('Thêm phụ tùng không thành công!');
        }
        this.flag = false;
      });
    }
  }
}
