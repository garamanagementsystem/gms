import { ComponentId } from "./../common/const/componentId.const";
import { Component, OnInit } from "@angular/core";
import { TabStateService } from "../_services/tab-state.service";
import { JQueryHelper } from "../common/helpers/jquery.helper";
@Component({
  moduleId: module.id,
  templateUrl: "task.component.html"
})
export class TaskComponent implements OnInit {

  constructor(private tabStateService: TabStateService) {}

  public ngOnInit(): void {
    JQueryHelper.showLoading();
    JQueryHelper.hideLoading();
  }
}
