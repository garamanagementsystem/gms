import * as moment from 'moment';

export class JsHelper {
    public static truncate(arg: string, n: number): string {
        return (arg.length > n) ? arg.substr(0, n - 1) + '...' : arg;
    }

    public static cloneObject<T>(sourceObject: T): T {
        return JSON.parse(JSON.stringify(sourceObject));
    }

    public static getRandomInt(min: number = 0, max: number): number {
        const minNum: number = Math.ceil(min);
        const maxNum: number = Math.floor(max);
        return Math.floor((Math.random() * (max - min)) + min);
    }

    public static windowHasParent(): boolean {
        return window.self !== window.top;
    }

    // tslint:disable-next-line:ban-types
    public static executeImmediately(callback: Function, timeout: number = 0): void {
        setTimeout(() => {
            callback();
        }, timeout);
    }

    // tslint:disable-next-line:ban-types
    public static executeWithInterval(callback: Function, interval: number): void {
        setInterval(() => {
            callback();
        }, interval);
    }

    public static normalizeTextInput(input: string): string {
        let val: string = input.trim().toLocaleLowerCase();
        val = val.split(' ').join('');
        return val;
    }

    public static isInputMatched(input: string, conditionCandidates: string[]): boolean {
        return conditionCandidates.some((cond) => cond && cond.toLocaleLowerCase().includes(input));
    }

    public static deepFreeze(o: any): any {
        Object.freeze(o);
        if (o === undefined) {
            return o;
        }

        Object.getOwnPropertyNames(o).forEach((prop) => {
            if (o[prop] !== null &&
                (typeof o[prop] === 'object' || typeof o[prop] === 'function') &&
                !Object.isFrozen(o[prop])) {
                JsHelper.deepFreeze(o[prop]);
            }
        });

        return o;
    }
}
