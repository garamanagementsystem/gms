import { JsHelper } from './js.helper';
import { KeyCode } from '../enums/keycode.enum';
import * as $ from 'jquery';

export class JQueryHelper {
    public static showElement(selector: string | any, displayValue: string = ''): void {
        displayValue === '' ? $(selector).show() : $(selector).css('display', displayValue);
    }

    public static hideElement(selector: string | any): void {
        $(selector).hide();
    }

    public static slideElement(selector: string | any, direction: string = 'down'): void {
        switch (direction) {
            case 'up':
                $(selector).slideUp();
                break;
            case 'down':
                $(selector).slideDown();
                break;
            default:
                $(selector).slideDown();
                break;
        }
    }

    public static toggleElement(
        selector: string | any, complete?: (this: any) => void, duration: number = 400): void {
        $(selector).slideToggle(duration, complete);
    }

    public static toggleClass(selector: string | any, className: string): void {
        $(selector).toggleClass(className);
    }

    public static findElement(container: string | any, target: string | any): JQuery {
        return $(container).find(target);
    }


    public static scrollToBottom(): void {
        const oneSecond: number = 1000;
        JsHelper.executeImmediately(() => {
            $('html, body').animate({ scrollTop: $('body').get(0).scrollHeight }, oneSecond);
        });
    }

    public static scrollToTop(): void {
        JsHelper.executeImmediately(() => {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    }

    public static blurInput(selector: string | any): void {
        $(selector).blur();
    }

    public static onClickOutside(
        target: string | any,
        func: any,
        excludeTargets: string[] = []): void {

        $(document).click((e: any) => {
            const container: JQuery = $(target);

            let inExcludeTarget: boolean = false;
            for (const val of excludeTargets) {
                if (container.is($(val))) {
                    inExcludeTarget = true;
                    break;
                }
            }

            if (!inExcludeTarget &&
                !container.is(e.target) &&
                container.has(e.target).length === 0) {
                func();
            }
        });
    }

    public static setElementProperty(
        element: string | any,
        prop: string,
        value: string | number | boolean): void {
        $(element).prop(prop, value);
    }

    public static getCurrentScrollPosition(): number {
        return $(window).scrollTop();
    }

    public static getWindowHeight(): number {
        return $(window).height();
    }

    public static getDocumentHeight(): number {
        return $(document).height();
    }

    public static setElementCss(classOrIdName: string | any, css: string, value: string): void {
        $(classOrIdName).css(css, value);
    }

    public static setElementsCssByJson(classOrIdName: string | any, css: any): void {
        $(classOrIdName).css(css);
    }

    public static triggerScrollEvent(func: any): void {
        $(window).scroll(func);
    }

    public static trigger(selector: string | any, event: string): void {
        setTimeout(() => {
            $(selector).trigger(event);
        }, 0);
    }

    public static setValueInput(selector: string | any, value: any): void {
        setTimeout(() => {
            $(selector).val(value);
        }, 0);
    }

    public static getValueInput(selector: string | any): string {
        return $(selector).val().toString();
    }

    public static addClass(selector: string | any, className: any): void {
        $(selector).addClass(className);
    }

    public static removeClass(selector: string | any, className: any): void {
        $(selector).removeClass(className);
    }

    public static hasClass(selector: string | any, className: any): boolean {
        return $(selector).hasClass(className);
    }

    // tslint:disable-next-line:ban-types
    public static onHover(selector: string | any, handlerIn: Function, handlerOut: Function): void {
        $(selector).hover((e) => {
            handlerIn(e);
        }, (e) => {
            handlerOut(e);
        });
    }

    public static isEmptyObject(obj: any): boolean {
        return $.isEmptyObject(obj);
    }

    public static onFocus(selector: string | any): void {
        $(selector).focus();
    }

    public static showLoading(selector: string = '.loading-page'): void {
        // About to show loading ...
        $(selector).css('display', 'flex');
    }

    public static hideLoading(selector: string = '.loading-page'): void {
        // About to hide loading ...
        $(selector).css('display', 'none');
    }

    public static showLocalLoading(selector: string = '.loading-local'): void {
        // About to show loading ...
        $(selector).css('display', 'flex');
    }

    public static hideLocalLoading(selector: string = '.loading-local'): void {
        // About to hide loading ...
        $(selector).css('display', 'none');
    }

    public static showOverlay(selector: string = '#overlay'): void {
        $(selector).fadeIn();
    }

    public static hideOverlay(selector: string = '#overlay'): void {
        $(selector).fadeOut();
    }

    public static isNumeric(event: any): boolean {
        if ((event.keyCode === KeyCode.Dot || event.charCode === KeyCode.Dot)
            || event.keyCode === KeyCode.Backspace
            || event.keyCode === KeyCode.Delete
            || event.keyCode === KeyCode.Left
            || event.keyCode === KeyCode.Right
            || event.keyCode === KeyCode.Tab
            || (event.keyCode >= KeyCode.Char0 && event.keyCode <= KeyCode.Char9)
            || (event.charCode >= KeyCode.Char0 && event.charCode <= KeyCode.Char9)) {
            return true;
        }
        return false;
    }

    public static getNameRootModule(): string {
        return $('#root-module').text();
    }
}
