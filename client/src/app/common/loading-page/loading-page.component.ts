import { Component, Input, OnInit, ViewChild } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'loading-page',
    templateUrl: './loading-page.component.html',
    styleUrls: ['./loading-page.component.css']
})
export class LoadingPageComponent implements OnInit {
    @Input('backdrop') public backdrop: boolean = true;
    @Input('className') public className: string = '';
    @Input('local') public local: boolean = false;
    @ViewChild('element') public element: any;

    constructor() { }

    public ngOnInit(): any {
        if (this.className !== '') {
            this.element.nativeElement.classList.add(this.className);
        }
    }
}
