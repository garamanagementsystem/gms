﻿import { Component, OnInit, ViewContainerRef } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { JQueryHelper } from '../common/helpers/jquery.helper';
import * as $ from 'jquery';
import { ToastComponent } from '../common/toast-notification/toast-notification.component';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    private toastComponent: ToastComponent;

    constructor(private userService: UserService, private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.toastComponent = new ToastComponent(toastr, vcr);
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers(); });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }
    public showLoading(): void {
        this.toastComponent.showSuccess('Hello!');
    }
}
